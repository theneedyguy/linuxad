#!/bin/bash

#RUN THIS SCRIPT AS SUDO!!! (sudo ./join)

#This script currently allows GUI login only with non-lightdm greeters.
#Elementary greeter and Unity greeter crash when trying to login.
#Tested window managers are: Mate and Gnome.


#Ubuntu Linux config and Win AD auto-join.
#Version: 0.96-1
#Author: Kevin Christen
#Created: 30.04.2015



#ADMIN PARAMETERS. DO NOT EDIT IF YOU DON'T KNOW WHAT YOU'RE DOING---------------------------

#ENTER NAME SERVER IP-ADDRESS:
nsip=192.168.1.5
#ENTER DOMAIN CONTROLLER IP-ADDRESS:
dcip=192.168.1.5
#ENTER HOSTNAME OF FILE SERVER THAT HOSTS SHARES:
fsname=masterchief
#ENTER DOMAIN CONTROLLER HOSTNAME (NOT FQDN! [MUST BE LOWERCASE]):
dc=masterchief
#ENTER DOMAIN SUFFIX (E.G. contoso.com [MUST BE LOWERCASE])
domsuf=spartan.intern
#ENTER SHORT DOMAIN NAME (E.G. contoso [MUST BE LOWERCASE])
shortdom=spartan

#END OF ADMIN PARAMETERS----------------------------------------------------------------------

cat << "EOF"

UBUNTU AD AUTO JOIN SCRIPT
Version 0.96-1

Changelog:
[Version 0.96-1]
    * Fixed spelling and an error in sudo checking
[Version 0.96]
    * Main file renamed to "join"
    * All former subfiles are now included in the main file
    * Bash script is now compiled to C to ensure that endusers cannot change the script
    * Thanks to C compilation we can now set sudo privileges for users
[Version 0.95]
	* Automatically configures mount of shares (IGDLA and such)
[Version 0.9]
	* Now including PAM GUI login
	* Creates userhome at login
	* Now reboots after completion to ensure working login
	* Moved admin parameters to top for easy configuration
[Version 0.5]
	* Initial working release
  
EOF
#Check if sudo
if [ "$(id -u)" != "0" ]; then
	echo "Sorry, you are not root. Resart the script with superuser privileges!"
	exit 1
fi
#Enter user credentials
echo "Enter your Domain Username":
read username

echo "Password":
read -s password
echo

#Display config for user
echo "Setting $nsip as the Name Server..."
echo "Setting $dcip as the Domain Controller IP..."
echo "Setting $fsname as the File Server..."
echo "Setting $dc as Domain Controller..."
SHORTDOM_=${shortdom^^}
echo "Set $domsuf as Domain Suffix..."
echo "Changing host entries..."

#Change host entries
echo 127.0.0.1 localhost > /etc/hosts
echo 127.0.1.1 $(hostname).$domsuf $(hostname) >> /etc/hosts
echo ::1 ip6-localhost ip6-loopback >> /etc/hosts
echo fe00::0 ip6-localnet >> /etc/hosts
echo ff00::0 ip6-mcastprefix >> /etc/hosts
echo ff02::1 ip6-allnodes >> /etc/hosts
echo ff02::2 ip6:allrouters >> /etc/hosts

echo "Installing essential packages... This will take a while..." 
apt-get update -qq && apt-get install -qq krb5-user krb5-config libkrb5-dev winbind samba cifs-utils smbclient libnss-winbind libpam-winbind libpam-krb5 libpam-ccreds -y

echo "Installed all files!"
echo "Setting up kerberos..."
#------------------------------------------------------------------------------------------
#. ./.krb_conf.sh
DOMSUF_UPPER=${domsuf^^}

cat > /etc/krb5.conf << EOF
[libdefaults]
    default_realm = $DOMSUF_UPPER

[realms]
    $DOMSUF_UPPER = {
        kdc = $dc.$domsuf
        admin_server = $dc.$domsuf
        default_domain = $DOMSUF_UPPER
    }

[domain_realm]
    .$domsuf = $DOMSUF_UPPER
    $domsuf = $DOMSUF_UPPER
EOF
#------------------------------------------------------------------------------------------

#------------------------------------------------------------------------------------------
echo "Setting up samba..."
#. ./.samba_conf.sh
SHORTDOM_UPPER=${shortdom^^}

cat > /etc/samba/smb.conf << EOF
[global]
	security = ads
        realm = $DOMSUF_UPPER
        password server = $dcip
        workgroup = $SHORTDOM_UPPER
        winbind separator = +
        idmap uid = 10000-20000
        idmap gid = 10000-20000
        winbind enum users = yes
        winbind enum groups = yes
        template homedir = /home/%D/%U
        template shell = /bin/bash
        client use spnego = yes
        client ntlmv2 auth = yes
        encrypt passwords = yes
        winbind use default domain = yes
        restrict anonymous = 2
EOF
#------------------------------------------------------------------------------------------
echo "Creating domain specific home folder..."
mkdir /home/$SHORTDOM_/
echo "Creating automount script..."
#. ./.shares_conf.sh
#------------------------------------------------------------------------------------------
touch /usr/bin/mountshares

cat > /usr/bin/mountshares << EOF
#!/usr/bin/env bash

#IMPORTANT: Leave 'gvfs-mount smb://\$USER@$fsname/' and only replace '[share]'.
#$USER variable 
#Create a new line for each share you want to connect. I.e.: gvfs-mount smb://\$USER@$fsname/public
gvfs-mount smb://\$USER@$fsname/bereiche
gvfs-mount smb://\$USER@$fsname/Userhomes/\$USER

EOF

chmod +x /usr/bin/mountshares

echo "Creating autostart conf..."
touch /usr/share/applications/mountshares.desktop

cat > /usr/share/applications/mountshares.desktop << EOF

[Desktop Entry]
Name=Automount for Domain Shares
Exec=/usr/bin/mountshares
Type=Application
Terminal=false

EOF
echo "Creating autostart link..."
cp /usr/share/applications/mountshares.desktop /etc/xdg/autostart/
#------------------------------------------------------------------------------------------
#Setting sudo rights for domain admins and domain users
echo "Checking if sudo rights are set for domain admins and users..."
if grep -q "domain" /etc/sudoers
then
    echo "Sudo already set. Skipping this step..."
else
    echo "Sudo not set. Setting things up..."
cat >> /etc/sudoers << EOF
%domain\ admins ALL=(ALL:ALL) ALL
%domain\ users ALL=/usr/bin/apt-get,/sbin/shutdown,/sbin/reboot,/sbin/halt,/sbin/ifup,/sbin/ifdown,/bin/kill   
EOF
fi
#------------------------------------------------------------------------------------------
#This is important for linux to work with AD login.
echo "Changing PAM config..."
sed -i '7s/.*/passwd:		compat winbind/' /etc/nsswitch.conf
sed -i '8s/.*/group:		compat winbind/' /etc/nsswitch.conf

echo "Restarting services..."
service winbind stop
service smbd restart
service winbind start

echo "Updating PAM authentication..."
echo -e "\nPlease make sure that every entry in the following dialog is checked!!!\n"
sleep 5
pam-auth-update

echo "Trying to connect to $domsuf..."
net ads join -U $username%$password

cat << EOF

Going to restart now.

EOF
sleep 10
reboot
